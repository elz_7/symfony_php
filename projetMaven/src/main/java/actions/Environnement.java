package actions;

import com.opensymphony.xwork2.ActionSupport;
import modele.CalculatriceDynamiqueDuFutur;
import modele.CalculatriceDynamiqueDuFuturImpl;
import org.apache.struts2.interceptor.ApplicationAware;

import java.util.Map;

public class Environnement extends ActionSupport implements ApplicationAware {
    private CalculatriceDynamiqueDuFutur calculatriceDynamiqueDuFutur;

    public CalculatriceDynamiqueDuFutur getCalculatriceDynamiqueDuFutur() {
        return calculatriceDynamiqueDuFutur;
    }

    @Override
    public void setApplication(Map<String, Object> map) {
        map.putIfAbsent("calculatrice",new CalculatriceDynamiqueDuFuturImpl());
        calculatriceDynamiqueDuFutur= (CalculatriceDynamiqueDuFutur) map.get("calculatrice");

    }
}