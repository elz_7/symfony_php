package actions;

import modele.exceptions.NonSupporteeException;

import java.util.Collection;

public class CalculDynamique extends Environnement {


    public Collection<String> getLesOperations() {
        return getCalculatriceDynamiqueDuFutur().getOperations();
    }

    private double operande1;
    private double operande2;
    private String operation;
    private double resultat;

    public long getCompteur() {

        // CalculatriceDynamiqueDuFutur getCalculatriceDynamiqueDuFutur;
        return getCalculatriceDynamiqueDuFutur().getValeurCompteur();
    }

    public double getOperande1() {
        return operande1;
    }

    public String getOperation() {
        return operation;
    }

    public double getOperande2() {
        return operande2;
    }

    public double getResultat() {
        return resultat;
    }

    public void setOperande1(double operande1) {
        this.operande1 = operande1;
    }

    public void setOperande2(double operande2) {
        this.operande2 = operande2;
    }

    public void setOperation(String operation) {
        this.operation=operation;
    }

    public void setResultat(double resultat) {
        this.resultat = resultat;
    }

    @Override
    public String execute() throws Exception {
        try {
            resultat=getCalculatriceDynamiqueDuFutur().doCalcul(operation,operande1,operande2);

        } catch (NonSupporteeException e) {
            addFieldError("operation", getText("erreur"));
            return INPUT;
        }


        return SUCCESS;
    }
}