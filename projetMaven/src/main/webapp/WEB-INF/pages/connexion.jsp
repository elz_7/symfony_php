<%@ taglib prefix="s" uri="/struts-tags" %>
<%--
  Created by IntelliJ IDEA.
  User: lunef
  Date: 18/02/2020
  Time: 16:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><s:text name="connexion.titre"> </s:text></title>
</head>
<body>
<s:form action="saisie">
    <s:textfield name="pseudo" key="connexion.login" />
    <s:password name="motSecret" key="connexion.password"/>
    <s:submit value="OK"> </s:submit>

</s:form>

</body>
</html>