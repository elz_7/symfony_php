<%@ taglib prefix="s" uri="/struts-tags" %>
<%--
  Created by IntelliJ IDEA.
  User: lunef
  Date: 18/02/2020
  Time: 16:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><s:text name="accueil.titre"> </s:text></title>
</head>
<body>
<ul>
    <li><s:a action="connexion"><s:text name ="accueil.connexion"/></s:a></li>
    <li><s:a action="calculatrice"><s:text name ="accueil.calculatrice"/></s:a></li>
    <li><s:a action="calculatriceDynamique"><s:text name ="accueil.dynamique"/></s:a></li>
    <li><s:a action="etudiant"><s:text name ="accueil.etudiants"/></s:a></li>
</ul>
</body>
</html>
