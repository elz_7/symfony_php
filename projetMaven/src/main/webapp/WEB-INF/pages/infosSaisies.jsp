<%@ taglib prefix="s" uri="/struts-tags" %>
<%--
  Created by IntelliJ IDEA.
  User: lunef
  Date: 18/02/2020
  Time: 16:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><s:text name="infos.saisie"></s:text></title>
</head>
<body>
<s:property value="pseudo"></s:property>
<s:property value="motSecret"></s:property>
<s:a action="accueil"><s:text name="retour"></s:text></s:a>

</body>
</html>
