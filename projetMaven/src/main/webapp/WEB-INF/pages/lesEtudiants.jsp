<%@ taglib prefix="s" uri="/struts-tags" %>
<%--
  Created by IntelliJ IDEA.
  User: lunef
  Date: 18/02/2020
  Time: 16:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><s:text name="lesEtudiants.titre"></s:text></title>
</head>
<body>
<ul>
    <s:iterator value="%{LesEtudiants}" var="etudiant">
        <li><s:property value="#etudiant.numeroEtudiant"/> <s:property value="nom"/> <s:property value="prenom"/> <s:property value="age"/> </li>
    </s:iterator>
</ul>

</body>
</html>
