<%@ taglib prefix="s" uri="/struts-tags" %>
<%--
  Created by IntelliJ IDEA.
  User: lunef
  Date: 18/02/2020
  Time: 16:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><s:text name="calculatriceDynamique.titre"></s:text></title>
</head>
<body>
<s:form action="calculDynamique" validate="true">
    <s:textfield name="operande1" key="calculatrice.operande1"></s:textfield>
    <s:textfield name="operande2" key="calculatrice.operande2"></s:textfield>
    <s:select list="%{lesOperations}" key="calculatrice.operation" name="operation"></s:select>
    <s:submit key="calculatrice.bouton"></s:submit>
</s:form>

<s:a action="accueil">Retour a l'accueill</s:a>
</body>
</html>
